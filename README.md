# BarCodes.cc

### Send and track promo codes

Barcodes is intended to be used with Google AdWords sitelinks. AdWords sitelinks are links that you can choose to show on specific days. If you need to track actual promo redemptions and not just link clicks, then Barcodes can track that.

For example, on Thursdays your ad will display a link - barcodes.cc/.../thursday - to a page where a customer can enter their phone number and receive a discount code by SMS. In the merchant admin backend, you can enter codes when they're used to track redemptions.