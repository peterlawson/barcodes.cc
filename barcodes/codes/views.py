from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse

from django.utils.timezone import now
from django.utils.crypto import get_random_string

from codes.models import Customer, Merchant, BarCode
from codes.forms import PhoneForm, CustomerForm

import twilio

import json
jsonDec = json.decoder.JSONDecoder()

from twilio.rest import Client as TwilioClient

from barcodes.settings import TWILIO_AUTH_TOKEN, TWILIO_SID

days_map = {'Sunday': 'SU', 'Monday': 'MO', 'Tuesday': 'TU', 'Wednesday': 'WE', 'Thursday': 'TH', 'Friday': 'FR', 'Saturday': 'SA'}


# def send_code(request, place_name, day, uuid):
#   return HttpResponse("You are here: {0}, {1}, {2}".format(place_name, day, uuid))

# def day_view(request, place_name, day):
#   return redirect('send_code', place_name=place_name, uuid=get_uuid())

def index(request):
  return HttpResponse("Index")

def request_code(request, merch_slug, day, phone_number=None):
  if request.method == 'POST':
    form = CustomerForm(request.POST)
    if form.is_valid():
      form_data = form.cleaned_data
      phone_number = form_data['phone_number']
      cx, created = Customer.objects.get_or_create(phone_number=phone_number)
      merch_object = Merchant.objects.get(slug=merch_slug)

      if BarCode.objects.filter(customer=cx, sent_on=now().date()):
        error_message = "You've already requested a code for {0} today.".format(merch_object.name)
        return render(
          request,
          'request_code_error.html',
          {'error_message': error_message})
      else: # CX hasn't made request for this merch today
        # Get merch codes; pop top code; convert new codes list to json and save.
        merch_codes_json = merch_object.codes
        codes_list = jsonDec.decode(merch_codes_json)
        code = codes_list.pop()
        merch_object.codes = json.dumps(codes_list)
        merch_object.save()

        # Create BarCode object
        day_tag = now().strftime("%A")
        new_barcode_object = BarCode.objects.create(
          customer=cx,
          code_tag=days_map[day_tag],
          merchant=merch_object,
          code_str=code)

        # Send code to CX via Twilio
        t_client = TwilioClient(TWILIO_SID, TWILIO_AUTH_TOKEN)
        sms_message = t_client.messages.create(
          to="+1" + phone_number,
          from_="+16154549878",
          body="Thanks for using Bar Codes! Show this code to your bartender or server to redeem the promotion: {0}".format(code))

        return render(
          request,
          'confirmation.html',
          {'phone_number': phone_number,
           'code': code})
    else: # Form invalid
      error_message = "Please enter a valid phone number."
      return render(
        request,
        'request_code_error.html',
        {'error_message': error_message})
  else: # Method is not POST
    if now().strftime("%A").lower() != day:
      error_message = "Sorry, today is {0}!".format(now().strftime("%A"))
      return render(
        request,
        'request_code_error.html',
        {'error_message': error_message})
    else: # Day requested is today
      merch_object = get_object_or_404(Merchant, slug=merch_slug)
      form = CustomerForm()
  return render(
    request,
    'request_code.html',
    {'form': form, 'merchant_name': merch_object.name, 'merch_slug': merch_slug, 'day': day})

def info_merchants(request):
  if request.method == 'GET':
    return render(request, 'info_merchants.html')

def info_customers(request):
  if request.method == 'GET':
    return render(request, 'info_customers.html')

def get_help(request):
  if request.method == 'GET':
    return render(request, 'help.html')
