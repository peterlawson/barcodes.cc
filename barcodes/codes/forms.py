from django import forms

from django.forms import ModelForm
from codes.models import Customer

class PhoneForm(forms.Form):
  phone_number = forms.CharField(label="Phone number", max_length=11, min_length=10, required=True)

class CustomerForm(ModelForm):
  class Meta:
    model = Customer
    fields = ['phone_number']