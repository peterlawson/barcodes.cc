from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<slug:merch_slug>/<str:day>/request-code/', views.request_code, name='request-code'),
    path('customers/', views.info_customers, name='info-customers'),
    path('merchants/', views.info_merchants, name='info-merchants'),
    path('help/', views.get_help, name='help'),
]