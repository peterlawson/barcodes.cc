from django.db import models

from django.core.validators import RegexValidator

import json
jsonDec = json.decoder.JSONDecoder()

# Create your models here.

class Customer(models.Model):
  phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
  phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=False) # validators should be a list

class Merchant(models.Model):
  name = models.CharField(max_length=50, unique=True)
  codes = models.TextField(null=True)
  slug = models.SlugField()

  # def get_code(self):
  #   codes_list = jsonDec(codes)
  #   c = codes_list.pop()
  #   self.codes = json.dumps(c)
  #   return c

# Codes go into database once they are sent to (i.e. requested by) a customer. Until then,
# they sit in an array on the Merchant model.
#
# The problem with this approach, however, is that this application is isolated from the resto's
# POS system. Barcodes.cc won't actually know the barcode has been redeemed.
# One solution to this would be to batch upload the redeemed codes from the POS into Barcodes.cc
# (once a month or so) and update the Barcodes.cc database.

class BarCode(models.Model):
  code_str = models.CharField(max_length=36)
  TAG_CHOICES = (
    ('SU', 'Sunday'),
    ('MO', 'Monday'),
    ('TU', 'Tuesday'),
    ('WE', 'Wednesday'),
    ('TH', 'Thursday'),
    ('FR', 'Friday'),
    ('SA', 'Saturday'),
    ('C1', 'Custom 1'),
    ('C2', 'Custom 2'),
    ('C3', 'Custom 3'),
    ('C4', 'Custom 4'),
    ('C5', 'Custom 5'),
  )
  code_tag = models.CharField(
    max_length=2,
    choices=TAG_CHOICES,
  )
  merchant = models.ForeignKey(
    'Merchant',
    on_delete=models.CASCADE
  )
  customer = models.ForeignKey(
    'Customer',
    on_delete=models.CASCADE
  )
  sent_on = models.DateField(auto_now_add=True)
  redeemed = models.BooleanField(default=False)
  redeemed_on = models.DateField(null=True)
